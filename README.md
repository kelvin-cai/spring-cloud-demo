# spring-cloud-demo

## 介绍
SpringCloud微服务开发样例

包含：

- eureka注册中心eureka-server

- 服务提供者

- 服务消费者或者说调用方config-client

- 服务消费者利用feign方式调用consumer-feign-without-hystrix

- 服务消费者利用feign+hystrix方式达到服务降级consumer-feign-with-hystrix

- restamplate+hystrix方式调用远端服务也可以实现，但是这个方式开发起来不简洁，在本作中没有写

- 配置中心服务config-server

- 配置中心消费者样例provider-user 

- 合并请求provider-hystrix-request-merge

## 软件架构
软件架构说明


当前版本搭配

springboot 1.4.1.RELEASE

springCloud Camden.SR2

## 升级springcloud
1. 如果要升级SpringCloud，请用最新版。

2. springcloud用Hoxton.SR3，springboot对应2.2.5.RELEASE。这里是2020年3月，如需实时更新，请到官
网spring.io找springcloud页面，页面内能找到版本对应关系。

3. 相比springboot1.4.1对应的springcloud Camden.SR2。发生了以下改变

eureka服务jar名改变、eureka客户端jar名改变、Feignjar名改变

使用eureka认证密码时，eureka的yml的security前要加spring.

## 安装教程

1. mvn clean build 

## 使用说明

1. 对eureka工程的Application.java的main函数执行run

2. 对user工程的Application.java的main函数执行run

3. 对consumser-feign-with-hystrix工程的Application.java的main函数执行run

4. 执行consumer里面的调用请求，则可调用user 

5. 配置中心
包含配置中心server，添加spring-cloud-starter-config，指定配置中心地址
启动config-server中的Application
配置中心client，需要添加心跳包依赖actuator，注意必须在bootstrap中填写配置中心server地址
启动config-client中的Application

6. 如果需要更改git中配置后刷新，请对config-client服务 执行他的URL ： ip端口/refresh
若要自动刷新，需要添加rabbitMq或者kafka继续改造


## 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)