package com.dizang.cloud.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
/**
 * @RefreshScope使用时，对当前服务执行 post   localhost:8081/refresh
 * @RefreshScope不能用@Configuration一起使用
 * @author kelvin
 *
 */
@RefreshScope
@RestController
public class TestController {

	@Value("${profile}")
	private String profile;
	
	@RequestMapping(value="/getP",method=RequestMethod.GET)
	public Object getProfiles() {
		return this.profile;
	}
}
