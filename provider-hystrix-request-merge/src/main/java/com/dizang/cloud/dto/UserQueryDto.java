package com.dizang.cloud.dto;

import java.util.concurrent.CompletableFuture;

import com.dizang.cloud.entity.User;

public class UserQueryDto {

    private Long id;
    
    /** CompletableFuture接口 */
    private CompletableFuture<User> completedFuture;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CompletableFuture<User> getCompletedFuture() {
        return completedFuture;
    }

    public void setCompletedFuture(CompletableFuture<User> completedFuture) {
        this.completedFuture = completedFuture;
    }
    
    
}
