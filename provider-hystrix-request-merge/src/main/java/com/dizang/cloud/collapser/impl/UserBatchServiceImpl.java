package com.dizang.cloud.collapser.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import org.springframework.stereotype.Component;

import com.dizang.cloud.entity.User;
import com.netflix.hystrix.HystrixCollapser.Scope;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCollapser;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

/**
 * 
 * @author kelvin.cai
 *
 */
@Component
public class UserBatchServiceImpl {

    @HystrixCollapser(batchMethod = "getUserBatchById",scope=Scope.GLOBAL,
            collapserProperties = {@HystrixProperty(name ="timerDelayInMilliseconds",value = "2000")})
    public Future<User> getUserById(Long id) {
        throw new RuntimeException("This method body should not be executed");
    }

    @HystrixCommand
    public List<User> getUserBatchById(List<Long> ids) {
        System.out.println("进入批量处理方法"+ids);
        List<User> ps = new ArrayList<User>();
        for (Long id : ids) {
            User p = new User();
            p.setId(id);
            p.setUsername("dizang"+id);
            ps.add(p);
        }
        return ps;
    }
}
