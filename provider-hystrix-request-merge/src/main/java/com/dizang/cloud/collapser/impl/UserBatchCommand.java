package com.dizang.cloud.collapser.impl;

import java.util.ArrayList;
import java.util.List;

import com.dizang.cloud.entity.User;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

public class UserBatchCommand extends HystrixCommand<List<User>> {
    private List<Long> ids;
    
    public UserBatchCommand(String commandGroupKey,List<Long> ids) {
          super(HystrixCommandGroupKey.Factory.asKey(commandGroupKey));
          this.ids = ids;
      }
    
    @Override
    protected List<User> run() throws Exception {
        System.out.println("UserBatchCommand进入批量处理方法"+ids);
        List<User> ps = new ArrayList<User>();
        for (Long id : ids) {
            User p = new User();
            p.setId(id);
            p.setUsername("dizang"+id);
            ps.add(p);
        }
        return ps;
    }
    

}
