package com.dizang.cloud.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ConfigServerApp {
	//启动后访问 http://localhost:8080/foobar/dev/master：foobar是应用名字，dev环境名字，master分支
	public static void main(String[] args) {
		SpringApplication.run(ConfigServerApp.class, args);
	}

}
